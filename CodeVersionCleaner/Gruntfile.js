'use strict';

module.exports = function(grunt) {
    require('time-grunt')(grunt);
     
    grunt.initConfig({              
        codeversioncleaner: {
            options: {              
                webdav_server: grunt.option('WEBDAV_SERVER'),
                webdav_username: grunt.option('WEBDAV_USERNAME'),
                webdav_password: grunt.option('WEBDAV_PASSWORD'),
                max_code_versions: grunt.option('MAX_CODE_VERSIONS')
            }
        }
    }); 
        
    grunt.loadTasks("./tasks");
    grunt.registerTask('default', ['codeversioncleaner']);
};